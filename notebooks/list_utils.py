def column_diff(lista1, lista2):
    return list(set(lista1).symmetric_difference(set(lista2)))

# https://www.kite.com/python/answers/how-to-get-the-difference-between-two-list-in-python
def column_diff_lc(list1, list2):
    list_difference = []
    for item in list1:
        if item not in list2:
            list_difference.append(item)
    return list_difference

def get_names_start_with(df, column_prefix):
    return [item for item in df.columns.values if item.startswith(column_prefix)]

# https://stackoverflow.com/questions/29294983/how-to-calculate-correlation-between-all-columns-and-remove-highly-correlated-on
def list_high_correlated_columns(df, threshold):
    import numpy as np
    corr_matrix = df.corr().abs()
    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
    # Find features with correlation greater than 0.95
    return [column for column in upper.columns if any(upper[column] > threshold)]
    