from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

def calculate_metrics(y_true, y_predicted, tipo='weighted', to_string=True, mostrar=True, texto=('Accuracy', 'Precision', 'Recall', 'F1')):
    accuracy = accuracy_score(y_true, y_predicted)
    precision = precision_score(y_true, y_predicted, average=tipo)
    recall = recall_score(y_true, y_predicted, average=tipo)
    f1 = f1_score(y_true, y_predicted, average=tipo)
    if to_string:
        string = "{a}: {v_a}\n{p}: {v_p}\n{r}: {v_r}\n{f1_t}: {v_f1}".format(a=texto[0], v_a=accuracy, p=texto[1], v_p=precision, r=texto[2], v_r=recall, f1_t=texto[3], v_f1=f1)
        if mostrar:
            print(string)
        return accuracy, precision, recall, f1, string
    else:
        return accuracy, precision, recall, f1

def plot_confusion_matrix(cm, labels=['0', '1']):
    ax= plt.subplot()
    sns.heatmap(cm, annot=True, ax = ax, cmap='Blues')

    # labels, title and ticks
    ax.set_xlabel('Predicted labels')
    ax.set_ylabel('True labels')
    if labels:
        ax.xaxis.set_ticklabels(labels)
        ax.yaxis.set_ticklabels(labels)

    ax.set_title('Confusion Matrix')