# MUSI-AA-PF-ML

Por: David López González

1a práctica final de la asignatura aprendizaje automático del máster MUSI

Esta consistes en procesar un conjunto de datos procedente de los apartamentos de Mallorca que están inscritos en Airbnb y después con este conjunto de datos generar un sistema de regresión o de clasificación para intentar predecir el precio de dichos apartamentos.

El proyecto ha sido estructurado en diferentes Notebooks para facilitar el desarrollo del proyecto y siguen la siguiente estructura:

1. **Preprocesamiento de los datos**: encargado de eliminar parte de las columnas y transformar otras para que estén en el formato deseado
2. **Procesamiento de los datos** encargado de realizar la imputación de nulos de las variables numéricas, el escalado de algunas columnas y preparar los tres conjuntos de datos
3. **Generación de modelos** encargado de generar diferentes modelos de ML y comparar los resultados
* **Big Notebook** contiene todos los notebooks anteriores en uno mismo, para facilitar la ejecución completa.

El proceso de ejecución se puede realizar mediante los 3 notebooks por separado o utilizando el Big Notebook.

Los PDFs se han generado a partir de los HTML, ya que la herramienta de exportación directa a PDF de Jupyter daba error.

El código esta pensado para ser ejecutado en Jupyter Lab 2.

Se ha utilizado el entorno ```ds-uib``` para la realización del proyecto. En caso de no disponerlo, se incluye el fichero ```.yml``` para recrear el entorno.